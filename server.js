var express    = require("express");

var login = require('./routes/loginroutes'); // for login/register user

var employee = require('./routes/employeeroutes'); // for employee crud

var bodyParser = require('body-parser');
var expressValidator = require('express-validator');

var app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(expressValidator());
app.use(bodyParser.json());


app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

var router = express.Router();

// test route
router.get('/', function(req, res) {
    res.json({ message: 'welcome to our upload module apis' });
});


//route to handle user registration/login
router.post('/register',login.register);
router.post('/login',login.login);
//router.post('/fblogin',login.fblogin);
router.post('/forgotpassword',login.forgotpass);
router.post('/resetpassword',login.resetpass);

//route to handle employee crud
router.post('/employees',employee.list);
router.post('/add_employee',employee.add);
router.post('/update_employee',employee.update);
router.post('/delete_employee',employee.delete);
router.post('/status_employee',employee.changestatus);


app.use('/api', router);

app.listen(3000);
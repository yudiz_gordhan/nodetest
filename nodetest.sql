-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 12, 2017 at 03:00 PM
-- Server version: 5.6.25
-- PHP Version: 5.6.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nodetest`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `changePassword` (IN `userid` INT(10), IN `password` VARCHAR(100))  BEGIN
	UPDATE users
    SET password = password,
    access_token = ''
    WHERE id = userid;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `checkUserAccess` (IN `user_email` VARCHAR(100), IN `otp` VARCHAR(10))  BEGIN
	SELECT * FROM users 
    WHERE email = user_email AND access_token = otp;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `check_fbuser` (IN `facebookId` INT(100))  BEGIN
	SELECT * FROM users WHERE facebook_id = facebookId;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_employe` (IN `employee_id` INT)  BEGIN
	DELETE FROM `employees`
    WHERE `id` = employee_id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getUserByEmail` (IN `user_email` VARCHAR(100))  BEGIN
	SELECT * FROM users WHERE email = user_email;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_employees` (IN `sort_by` VARCHAR(100), IN `st_r` INT(100), IN `search` VARCHAR(100), IN `t_r` INT)  BEGIN
	SELECT * FROM employees ORDER BY sort_by LIMIT st_r,t_r;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_employees2` (IN `a` VARCHAR(256), IN `b` VARCHAR(256))  select * from employees ORDER BY a LIMIT 0,10$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_employe` (IN `first_name` VARCHAR(100), IN `last_name` VARCHAR(100), IN `email` VARCHAR(100), IN `mobile` VARCHAR(100), IN `city` VARCHAR(100), IN `state` VARCHAR(100), IN `country` VARCHAR(100), IN `dob` DATE, IN `create_at` DATETIME, IN `status` INT)  BEGIN
  INSERT INTO employees (first_name, last_name, email, mobile, city, state, country, dob, create_at, status) VALUES (first_name, last_name, email, mobile, city, state, country, dob, create_at, status);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_users` (IN `first_name` VARCHAR(100), IN `last_name` VARCHAR(100), IN `username` VARCHAR(100), IN `email` VARCHAR(100), IN `password` VARCHAR(255), IN `created` DATETIME)  BEGIN
  insert into users(first_name, last_name, username, email, password, created)
  values(first_name, last_name, username, email, password, created);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `login_user` (IN `user_email` VARCHAR(100))  BEGIN
  SELECT password FROM users where email = user_email;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `set_OTP` (IN `user_email` VARCHAR(100), IN `OTP` VARCHAR(100))  BEGIN
UPDATE users
SET access_token = OTP
WHERE email = user_email;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `status_employe` (IN `employee_id` INT(10), IN `new_status` INT(10))  BEGIN
UPDATE employees
SET STATUS = new_status
WHERE id =employee_id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `update_employe` (IN `employee_id` INT, IN `first_name` VARCHAR(100), IN `last_name` VARCHAR(100), IN `email` VARCHAR(100), IN `mobile` VARCHAR(100), IN `city` VARCHAR(100), IN `state` VARCHAR(100), IN `country` VARCHAR(100), IN `dob` DATE)  NO SQL
BEGIN
UPDATE employees
SET first_name = first_name,
	last_name = last_name,
    email = email,
    mobile = mobile,
    city = city,
    state = state,
    country = country,
    dob = dob
WHERE id = employee_id;
END$$

--
-- Functions
--
CREATE DEFINER=`root`@`localhost` FUNCTION `getResultValue` () RETURNS VARCHAR(255) CHARSET latin1 return (SELECT * FROM employees)$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(11) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `mobile` varchar(100) NOT NULL,
  `city` varchar(100) DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `dob` date NOT NULL,
  `create_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `first_name`, `last_name`, `email`, `mobile`, `city`, `state`, `country`, `status`, `dob`, `create_at`, `updated_at`) VALUES
(1, 'dsf', 'sdf', 'gordhan@itindia.co.in', '423434', 'dfgdfg', 'dgdf', 'dfgdf', 1, '1989-01-23', '2017-07-07 17:52:07', '2017-07-11 05:23:08'),
(2, 'mohit1', 'bhesaniya', 'mohit@gmail.com', '1234567890', 'Ahmedabad', 'Gujarat', 'India', 0, '1992-01-23', '2017-07-07 17:53:26', '2017-07-12 06:40:41'),
(4, 'mohit2', 'bhesaniya', 'mohit2@gmail.com', '1234567890', 'Ahmedabad', 'Gujarat', 'India', 1, '1992-01-23', '2017-07-11 12:37:28', '2017-07-11 07:07:27');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `facebook_id` int(100) NOT NULL,
  `access_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `username`, `email`, `password`, `facebook_id`, `access_token`, `created`, `modified`) VALUES
(1, 'gz', 'chauhan', 'testuser', 'gordhan@itindia.co.in', 'sha1$dbb3a913$1$f0d6371932796754988294f3e740e1df3402b5fb', 0, '', '2017-07-07 15:02:38', '2017-07-12 10:57:28'),
(6, 'gz', 'chauhan', 'testuser1', 'gzchauhan1@gmail.com', 'sha1$96661b82$1$a32ba697d116de757ae742f85de5b6f0c1fe283c', 0, '', '2017-07-07 16:55:52', '2017-07-07 11:25:52'),
(7, 'gz1', 'chauhan1', 'testuser12', 'gzchauhan12@gmail.com', 'sha1$80111f13$1$db269d09f57ad5a8f2cd2b3f2b5dfc90a96195b2', 0, '', '2017-07-12 12:32:19', '2017-07-12 07:02:19');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

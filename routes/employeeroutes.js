var expressValidator = require('express-validator');
var db = require('../db');

// for employee list
exports.list = function(req,res){

  var sortBy = req.body.sort;
  var sortOrder = req.body.order;
  var search = req.body.search;
  var iDisplayStart = req.body.start;
  var iDisplayLength = req.body.lenght;
  var sort_by = sortBy+' '+sortOrder;
  var filters = [sort_by,iDisplayStart,search,iDisplayLength];
  //console.log(filters);return false;
  db.query('call get_employees(?,?,?,?)',filters, function (error, results, fields) {

    if (error) {
      res.send({
        "code":400,
        "failed":error
      })
    }else{
      
      var reslt = JSON.stringify(results[0]);
      var resultJson =  JSON.parse(reslt);
      console.log(resultJson);
      if(resultJson.length > 0){
          
          res.send({
            "code":200,
            "data":resultJson,
            "success":"login sucessfull"
          });
      }
      else{
        res.send({
          "code":400,
          "success":"No record found"
        });
      }
    }
  });
}

// for add new employee
exports.add = function(req,res){
  
  var today = new Date();

  var first_name = req.body.first_name;
  var last_name = req.body.last_name;
  var email = req.body.email;
  var mobile = req.body.mobile;
  var city = req.body.city;
  var state = req.body.state;
  var country = req.body.country;
  var dob = req.body.dob;
  var status = 1;
  
  //Validations
  req.checkBody('first_name', 'First Name is Required').notEmpty();
  req.checkBody('last_name', 'Last Name is Required').notEmpty();
  req.checkBody('email', 'Email is Required').notEmpty();
  req.checkBody('email', 'Please Enter a valid E-mail Address').isEmail();
  req.checkBody('mobile', 'Mobile is Required').notEmpty();
  req.checkBody('city', 'City is Required').notEmpty();
  req.checkBody('state', 'State is Required').notEmpty();
  req.checkBody('country', 'Country is Required').notEmpty();
  req.checkBody('dob', 'Birthdate is Required').notEmpty();

  var errors = req.validationErrors();

  if (errors) {
      res.send({
        "code":400,
        "failed":errors
      });
  }
  else{
    
    var employee=[
      first_name,
      last_name,
      email,
      mobile,
      city,
      state,
      country,
      dob,
      today,
      status
    ];
    //console.log(employee);return false;
    db.query('call insert_employe(?,?,?,?,?,?,?,?,?,?)',employee, function (error, results, fields) {

      if (error) {
        console.log("error ocurred",error);
        res.send({
          "code":400,
          "failed":error
        })
      }else{
        console.log('The solution is: ', results);
        res.send({
          "code":200,
          "success":"employee added sucessfully"
        });
      }
    });
  }
}

// for update employee
exports.update = function(req,res){
  
  var employee_id = req.body.employee_id;
  var first_name = req.body.first_name;
  var last_name = req.body.last_name;
  var email = req.body.email;
  var mobile = req.body.mobile;
  var city = req.body.city;
  var state = req.body.state;
  var country = req.body.country;
  var dob = req.body.dob;
  
  
  //Validations
  req.checkBody('employee_id', 'Employee id is Required').notEmpty();
  req.checkBody('first_name', 'First Name is Required').notEmpty();
  req.checkBody('last_name', 'Last Name is Required').notEmpty();
  req.checkBody('email', 'Email is Required').notEmpty();
  req.checkBody('email', 'Please Enter a valid E-mail Address').isEmail();
  req.checkBody('mobile', 'Mobile is Required').notEmpty();
  req.checkBody('city', 'City is Required').notEmpty();
  req.checkBody('state', 'State is Required').notEmpty();
  req.checkBody('country', 'Country is Required').notEmpty();
  req.checkBody('dob', 'Birthdate is Required').notEmpty();

  var errors = req.validationErrors();

  if (errors) {
      res.send({
        "code":400,
        "failed":errors
      });
  }
  else{
    
    var employee=[
      employee_id,
      first_name,
      last_name,
      email,
      mobile,
      city,
      state,
      country,
      dob
    ];
    //console.log(employee);return false;
    db.query('call update_employe(?,?,?,?,?,?,?,?,?)',employee, function (error, results, fields) {

      if (error) {
        console.log("error ocurred",error);
        res.send({
          "code":400,
          "failed":error
        })
      }else{
        console.log('The solution is: ', results);
        res.send({
          "code":200,
          "success":"employee updated sucessfully"
        });
      }
    });
  }
}

// for change employee status
exports.changestatus = function(req,res){
  
  var employee_id = req.body.employee_id;
  var status = req.body.status;  
  
  //Validations
  req.checkBody('employee_id', 'Employee id is Required').notEmpty();
  req.checkBody('status', 'First Name is Required').notEmpty();
  
  var errors = req.validationErrors();

  if (errors) {
      res.send({
        "code":400,
        "failed":errors
      });
  }
  else{
    
    var employee=[
      employee_id,
      status
    ];
    //console.log(employee);return false;
    db.query('call status_employe(?,?)',employee, function (error, results, fields) {

      if (error) {
        console.log("error ocurred",error);
        res.send({
          "code":400,
          "failed":error
        })
      }else{
        console.log('The solution is: ', results);
        res.send({
          "code":200,
          "success":"employee status changed sucessfully"
        });
      }
    });
  }
}

// for delete employee
exports.delete = function(req,res){
  var employee_id = req.body.employee_id;
  
  
  //Validations
  req.checkBody('employee_id', 'Employee id is Required').notEmpty();
  
  var errors = req.validationErrors();

  if (errors) {
      res.send({
        "code":400,
        "failed":errors
      });
  }
  else{
    
    var employee=[
      employee_id
    ];
    //console.log(employee);return false;
    db.query('call delete_employe(?)',employee, function (error, results, fields) {

      if (error) {
        console.log("error ocurred",error);
        res.send({
          "code":400,
          "failed":error
        })
      }else{
        console.log('The solution is: ', results);
        res.send({
          "code":200,
          "success":"employee deleted sucessfully"
        });
      }
    });
  }
}
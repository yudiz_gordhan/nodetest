var passwordHash = require('password-hash');
var expressValidator = require('express-validator');
var randomString = require('random-string');
var db = require('../db');
var transporter = require('../SMTP');

// for register
exports.register = function(req,res){
  
  var today = new Date();

  var first_name = req.body.first_name;
  var last_name = req.body.last_name;
  var email = req.body.email;
  var password = req.body.password;
  var confirm_password = req.body.confirm_password;
  var username = req.body.username;

  //Validations
  req.checkBody('first_name', 'First Name is Required').notEmpty();
  req.checkBody('last_name', 'Last Name is Required').notEmpty();
  req.checkBody('username', 'Username is Required').notEmpty().isAlphanumeric().len(3,20);
  req.checkBody('email', 'Email is Required').notEmpty();
  req.checkBody('email', 'Please Enter a valid E-mail Address').isEmail();
  req.checkBody('password', 'Password is Required').notEmpty();
  req.checkBody('vNewPassword', 'Password should be more than 5 chars and less than 20 chars').len(5,20);
  req.checkBody('confirm_password', 'Confirm Password is Required').notEmpty().len(5,20);
  req.checkBody('confirm_password', 'Passwords do not match').equals(password); // check password

  var errors = req.validationErrors();

  if (errors) {
      res.send({
        "code":400,
        "failed":errors
      });
  }
  else{
    
    var users=[
      req.body.first_name,
      req.body.last_name,
      req.body.username,
      req.body.email,
      passwordHash.generate(req.body.password),
      today
    ];

    db.query('call insert_users(?,?,?,?,?,?)',users, function (error, results, fields) {

      if (error) {
        console.log("error ocurred",error);
        res.send({
          "code":400,
          "failed":error
        })
      }else{
        console.log('The solution is: ', results);
        res.send({
          "code":200,
          "success":"user registered sucessfully"
        });
      }
    });
  }

}

// for login
exports.login = function(req,res){
  
  var user_email= req.body.email;
  var password = req.body.password;
    
  var email = req.body.email;
  var password = req.body.password;

  //Validations
  req.checkBody('email', 'Email is Required').notEmpty();
  req.checkBody('email', 'Please Enter a valid E-mail Address').isEmail();
  req.checkBody('password', 'Password is Required').notEmpty();

  var errors = req.validationErrors();

  if (errors) {
      res.send({
        "code":400,
        "failed":errors
      })
  }
  else{
      
      db.query('call login_user(?)',[user_email], function (error, results, fields) {

        if (error) {
          res.send({
            "code":400,
            "failed":"error ocurred"
          })
        }else{
          
          if(results[0].length > 0){
              
              var reslt = JSON.stringify(results[0]);
              var json =  JSON.parse(reslt);
              var userPass = json[0].password;
              
            if(passwordHash.verify(password, userPass)){
              res.send({
                "code":200,
                "success":"login sucessfull"
                  });
            }
            else{
              res.send({
                "code":204,
                "success":"Email and password does not match"
                  });
            }
          }
          else{
            res.send({
              "code":204,
              "success":"Email does not exits"
            });
          }
        }
      });
  }  
}

// for facebook login
// exports.fblogin = function(req,res){
  
//   var user_email= req.body.email;
//   var facebook_id = req.body.facebook_id;
//   var facebookId = facebook_id;
//   var email = req.body.email;
//   var name = req.body.name;
  
//   //Validations
//   req.checkBody('facebook_id', 'facebook id is Required').notEmpty();
  
//   var errors = req.validationErrors();

//   if (errors) {
//       res.send({
//         "code":400,
//         "failed":errors
//       })
//   }
//   else{
      
//       db.query('call check_fbuser(?)',[facebookId], function (error, results, fields) {

//         if (error) {
//           res.send({
//             "code":400,
//             "failed":"error ocurred"
//           })
//         }else{
          
//           if(results[0].length > 0){
              
//               res.send({
//                 "code":200,
//                 "success":"login sucessfull"
//               });
//           }
//           else{
            
//           }
//         }
//       });
//   }  
// }

// for send forgot password OTP
exports.forgotpass = function(req,res){
  
  var user_email= req.body.email;
    
  var email = req.body.email;
  
  //Validations
  req.checkBody('email', 'Email is Required').notEmpty();
  req.checkBody('email', 'Please Enter a valid E-mail Address').isEmail();
  
  var errors = req.validationErrors();

  if (errors) {
      res.send({
        "code":400,
        "failed":errors
      })
  }
  else{
    // check user available or not
    var checkUser = db.query('call getUserByEmail(?)',[user_email],function(error, results, fields){
        if (error) {
          console.log("error ocurred",error);
          res.send({
            "code":400,
            "failed":error
          })
        }else{
          
          if(results[0].length > 0)
          {
            var randomPassword = randomString({length: 6,letters:true}); // generate OTP
    
            var mailOptions = {
                from: 'gordhan@itindia.co.in', // sender address
                to: email, // list of receivers
                subject: 'One time password', // Subject line
                html: 'Here is your one time password <b>' + randomPassword + '</b>'// html body
            };
            transporter.sendMail(mailOptions, (error, info) => {
                if (error) {
                    return console.log(error);
                } else {
                    var data = [user_email, randomPassword];
                    db.query('call set_OTP(?,?)',data, function (error, results, fields) {

                      if (error) {
                        console.log("error ocurred",error);
                        res.send({
                          "code":400,
                          "failed":error
                        })
                      }else{
                        console.log('The solution is: ', results);
                        res.send({
                          "code":200,
                          "success":"One time password sent to your email id"
                        });
                      }
                    });
                }
            });
          }
          else{
            res.send({
              "code":400,
              "failed":"User not found"
            }) 
          }
        }
    });
    
  }
}

// for set password
exports.resetpass = function(req,res){
  var user_email= req.body.email;
  var otp = req.body.otp;
  var email = req.body.email;
  var password = req.body.password;
  var confirm_password = req.body.confirm_password;

  //Validations
  req.checkBody('email', 'Email is Required').notEmpty();
  req.checkBody('email', 'Please Enter a valid E-mail Address').isEmail();
  req.checkBody('otp', 'OTP is Required').notEmpty();
  req.checkBody('password', 'Password is Required').notEmpty();
  req.checkBody('password', 'Password should be more than 5 chars and less than 20 chars').len(5,20);
  req.checkBody('confirm_password', 'Confirm Password is Required').notEmpty();
  req.checkBody('confirm_password', 'Passwords do not match').equals(password); // check password
  
  var errors = req.validationErrors();

  if (errors) {
      res.send({
        "code":400,
        "failed":errors
      })
  }
  else{
    var checkUser = db.query('call checkUserAccess(?,?)',[user_email,otp],function(error, results, fields){
      if (error) {
        console.log("error ocurred",error);
        res.send({
          "code":400,
          "failed":error
        })
      }else{
        
        if(results[0].length > 0)
        {

          var reslt = JSON.stringify(results[0]);
          var resultJson =  JSON.parse(reslt);
          var userID = resultJson[0].id;
          
          // change password
          db.query('call changePassword(?,?)',[userID,passwordHash.generate(password)],function(error, results, fields){
            if (error) {
              console.log("error ocurred",error);
              res.send({
                "code":400,
                "failed":error
              })
            }else{
              res.send({
                "code":200,
                "success":"Password successfully changed."
              });
              
            }
          });
        }
        else{
          res.send({
            "code":400,
            "failed":"Email or OTP wrong"
          }) 
        }
      }
    });
  }
}

var mysql = require('mysql');

var dbConfig = {
                  host     : 'localhost',
                  user     : 'root',
                  password : 'root',
                  database : 'nodetest'  
                };

var connection = mysql.createConnection(dbConfig);

connection.connect(function(err){
  if(!err) {
    console.log("Database is connected ... nn");
  } else {
    console.log("Error connecting database ... nn");
  }
});
module.exports = connection; 